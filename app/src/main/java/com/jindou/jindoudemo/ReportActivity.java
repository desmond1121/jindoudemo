package com.jindou.jindoudemo;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;

public class ReportActivity extends AppCompatActivity {

    public static final String EXTRA_IMG = "EXTRA_IMG";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        setTitle("Report");

        byte[] byteArray = getIntent().getByteArrayExtra(EXTRA_IMG);
        Bitmap bitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
        ImageView iv = (ImageView) findViewById(R.id.report_image);
        iv.setImageBitmap(bitmap);
    }
}
