package com.jindou.jindoudemo.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

import com.jindou.jindoudemo.R;
import com.jindou.jindoudemo.Utils;

/**
 * Created by desmond on 3/25/17.
 */
public class StepView extends LinearLayout {

    public StepView(Context context) {
        this(context, null);
    }

    public StepView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public StepView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        setOrientation(HORIZONTAL);

        if (attrs != null) {
            TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.StepView);

            if (array != null) {
                int total = array.getInt(R.styleable.StepView_total, 0);
                int current = array.getInt(R.styleable.StepView_current, 0);
                int dimen = Utils.dp2px(context, 8);

                for (int i = 0; i < total; i++) {
                    View view = new View(context);
                    LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(dimen, dimen);
                    if (i < total - 1) {
                        lp.setMargins(0, 0, dimen, 0);
                    }
                    int res = i == current ? R.drawable.shape_step_solid : R.drawable.shape_step_gray;
                    view.setBackgroundDrawable(context.getResources().getDrawable(res));
                    addViewInLayout(view, i, lp);
                }

                array.recycle();
            }
        }
    }

}
