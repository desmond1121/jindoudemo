package com.jindou.jindoudemo;

import android.content.Context;
import android.content.res.Resources;

import java.io.Closeable;
import java.io.IOException;

/**
 * Created by desmond on 3/25/17.
 */
public class Utils {

    public static int dp2px(Context context, int dp) {
        Resources resources = context.getResources();
        float density = resources.getDisplayMetrics().density;
        return (int) (dp * density + 0.5f);
    }

    public static void closeQuietly(Closeable closeable) {
        if (closeable == null) {
            return;
        }
        try {
            closeable.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
