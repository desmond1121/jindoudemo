package com.jindou.jindoudemo;

import android.Manifest;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.jindou.jindoudemo.scan.ScanFragment;
import com.jindou.jindoudemo.scan.Step1Fragment;
import com.jindou.jindoudemo.scan.Step2Fragment;

import java.util.ArrayList;
import java.util.List;

public class ScanActivity extends AppCompatActivity {
    public static final int REQUEST_CAMERA = 0;

    public static final int STEP_STEP1 = 0;
    public static final int STEP_STEP2 = 1;
    public static final int STEP_SCAN = 2;

    private int mCurrentStep = STEP_STEP1;
    private final List<Fragment> mFragments = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (ContextCompat.checkSelfPermission(this,
            Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.CAMERA},
                REQUEST_CAMERA);
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCurrentStep >= STEP_STEP1) {
                    mCurrentStep--;
                }
                onBackPressed();
            }
        });

        mFragments.add(new Step1Fragment());
        mFragments.add(new Step2Fragment());
        mFragments.add(new ScanFragment());

        getSupportFragmentManager().beginTransaction()
            .add(R.id.step_container, mFragments.get(mCurrentStep))
            .commit();
    }

    public void goToStep(int step) {
        if (step > STEP_SCAN || step < STEP_STEP1 || mCurrentStep >= step) {
            return;
        }
        mCurrentStep = step;
        getSupportFragmentManager().beginTransaction()
            .replace(R.id.step_container, mFragments.get(step))
            .addToBackStack("Scan")
            .commit();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CAMERA) {
            if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "You must grant camera permission!", Toast.LENGTH_LONG).show();
                this.finish();
            }
        }
    }
}
