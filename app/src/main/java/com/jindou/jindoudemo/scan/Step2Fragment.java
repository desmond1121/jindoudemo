package com.jindou.jindoudemo.scan;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jindou.jindoudemo.R;
import com.jindou.jindoudemo.ScanActivity;

/**
 * Created by desmond on 3/25/17.
 */
public class Step2Fragment extends Fragment {

    public static final int COUNT_DOWN_DELAY = 1000;

    private TextView mCountDownText;
    private int mCountDown = 60;
    private Handler mHandler;
    private CountDownRunnable mRunnable = new CountDownRunnable();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mHandler = new Handler(Looper.getMainLooper());
        getActivity().setTitle("STEP 2");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_step2, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mCountDownText = (TextView) view.findViewById(R.id.count_down_text);
        mCountDownText.setText(String.valueOf(mCountDown));
        mHandler.postDelayed(mRunnable, COUNT_DOWN_DELAY);
        view.findViewById(R.id.scan_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goScan();
            }
        });
    }

    @Override
    public void onDestroyView() {
        mHandler.removeCallbacks(mRunnable);
        super.onDestroyView();
    }

    private void goScan() {
        mHandler.removeCallbacks(mRunnable);
        getScanActivity().goToStep(ScanActivity.STEP_SCAN);
    }

    private ScanActivity getScanActivity() {
        return (ScanActivity) getActivity();
    }

    private class CountDownRunnable implements Runnable {

        @Override
        public void run() {
            mCountDown--;
            if (mCountDown == 0) {
                goScan();
                return;
            }
            mCountDownText.setText(String.valueOf(mCountDown));
            mHandler.postDelayed(this, COUNT_DOWN_DELAY);
        }
    }

}
