package com.jindou.jindoudemo.scan;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.jindou.jindoudemo.CameraUtil;
import com.jindou.jindoudemo.R;
import com.jindou.jindoudemo.ReportActivity;
import com.jindou.jindoudemo.Utils;
import com.jindou.jindoudemo.widgets.CameraPreview;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;

/**
 * Created by desmond on 3/25/17.
 */
public class ScanFragment extends Fragment {

    public static final String TAG = "ScanFragment";

    private float mWindowX, mWindowY, mWindowWidth, mWindowHeight;
    private Camera mCamera;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle("STEP 3");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_scan, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mCamera = Camera.open();
        CameraPreview preview = new CameraPreview(getActivity(), mCamera);
        final FrameLayout container = (FrameLayout) view.findViewById(R.id.scan_container);
        final View window = view.findViewById(R.id.scan_window);

        container.addView(preview, 0);

        container.post(new Runnable() {
            @Override
            public void run() {
                int[] containerLoc = new int[2];
                container.getLocationOnScreen(containerLoc);
                int[] windowLoc = new int[2];
                window.getLocationOnScreen(windowLoc);
                mWindowX = ((float) (windowLoc[0] - containerLoc[0])) / container.getWidth();
                mWindowY = ((float) (windowLoc[1] - containerLoc[1])) / container.getHeight();
                mWindowWidth = ((float) window.getWidth()) / container.getWidth();
                mWindowHeight = ((float) window.getHeight()) / container.getHeight();
                Log.i(TAG, mWindowX + ", " + mWindowY + ", " + mWindowWidth + ", " + mWindowHeight);
            }
        });

        view.findViewById(R.id.scan_btn).setOnClickListener(
            new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mCamera.takePicture(null, null, new JpegCallback());
                }
            }
        );
    }

    @Override
    public void onDestroyView() {
        mCamera.stopPreview();
        mCamera.release();
        super.onDestroyView();
    }

    private class JpegCallback implements Camera.PictureCallback {

        @Override
        public void onPictureTaken(byte[] data, Camera camera) {

            BitmapFactory.Options options = new BitmapFactory.Options();

            Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length, options);
            Matrix matrix = new Matrix();
            matrix.postRotate(90);
            bitmap = Bitmap.createBitmap(bitmap ,
                0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
            Log.i(TAG, bitmap.getWidth() + "," + bitmap.getHeight());

            int x = (int) (mWindowX * bitmap.getWidth());
            int y = (int) (mWindowY * bitmap.getHeight());
            int width = (int) (mWindowWidth * bitmap.getWidth());
            int height = (int) (mWindowHeight * bitmap.getHeight());
            Log.i(TAG, x + ", " + y + ", " + width + ", " + height);


            int[] targetPixels = new int[width * height];
            bitmap.getPixels(targetPixels, 0, width, x, y, width, height);
            Bitmap target = Bitmap.createBitmap(targetPixels, 0, width, width, height, Bitmap.Config.ARGB_8888);
            ByteArrayOutputStream bos = null;
            try {
                bos = new ByteArrayOutputStream();
                target.compress(Bitmap.CompressFormat.JPEG, 100, bos);
                byte[] byteArray = bos.toByteArray();
                Intent intent = new Intent(getActivity(), ReportActivity.class);
                intent.putExtra(ReportActivity.EXTRA_IMG, byteArray);
                startActivity(intent);
            } finally {
                Utils.closeQuietly(bos);
            }
            bitmap.recycle();
            target.recycle();
        }
    }
}
